<?php
/**
 * Date: 10/08/18
 * Time: 22:32
 */

header('Content-Type: application/json');

session_start();

if (isset($_SESSION["SESSIONID"]) && $_SESSION["username"]){
    unset($_SESSION["SESSIONID"]);
    unset($_SESSION["username"]);
    session_write_close();
    echo json_encode(array("success" => true));
} else {
    echo json_encode(array("success" => false));
    header('HTTP/1.1 401 Unauthorized');
}