<?php
/**
 * Date: 10/08/18
 * Time: 22:19
 */

header('Content-Type: application/json');

session_start();
if (isset($_SESSION["SESSIONID"]) && $_SESSION["username"]){
    echo json_encode(array("success" => false, "error" => "you are already logged."));
    die();
}
if (!empty($_GET["username"]) && !empty($_GET["password"])) {
    $username = "root";
    $password = "";
    $host = "localhost";
    $dbname = "testing";

    $conn = new mysqli($host, $username, $password, $dbname);
    mysqli_set_charset($conn, "latin1_swedish_ci");

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } else {
        //echo "Connected.";
    }

    $sql = "SELECT * FROM users WHERE `username` = '" . $_GET["username"] . "'";
    if (!$result = mysqli_query($conn, $sql)) {
        echo(json_encode(array("success" => false)));
        header('HTTP/1.1 500 Internal Server Error');
        die();
    }
    $sqlArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $sqlArray[] = $row;
    }

    if (sizeOf($sqlArray) == 1) {
        if (password_verify($_GET["password"], $sqlArray[0]["password"])) {
            echo json_encode(array("success" => true));
                $_SESSION["SESSIONID"] = time();
                $_SESSION["username"] = $_GET["username"];
        } else {
            header('HTTP/1.1 401 Unauthorized');
            echo json_encode(array("success" => false));
        }
    } else {
        header('HTTP/1.1 404 Not Found');
        echo json_encode(array("success" => false));
    }
} else{
    echo json_encode(array("success" => false));
    header('HTTP/1.1 400 Bad Request');
}