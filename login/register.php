<?php
/**
 * Date: 10/08/18
 * Time: 22:26
 */

header('Content-Type: application/json');

if (isset($_GET["username"]) && isset($_GET["password"])) {
    $username = "root";
    $password = "";
    $host = "localhost";
    $dbname = "testing";

    $conn = new mysqli($host, $username, $password, $dbname);
    mysqli_set_charset($conn, "latin1_swedish_ci");

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } else {
        //echo "Connected.";
    }

    $sql = "SELECT * FROM users WHERE `username` = '" . $_GET["username"] . "'";
    if (!$result = mysqli_query($conn, $sql)) {
        echo(json_encode(array("success" => false)));
        header('HTTP/1.1 500');
        die();
    }
    $sqlArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $sqlArray[] = $row;
    }

    if (sizeOf($sqlArray) == 0) {
        $password = password_hash($_GET["password"], PASSWORD_DEFAULT);
        $sql = "INSERT INTO users(`username`, `password`,`type`) VALUES ('" . $_GET["username"] . "','" . $password . "',1)";
        //echo $sql;
        if (!$result = mysqli_query($conn, $sql)) {
            echo json_encode(array("success" => false));
            header('HTTP/1.1 500 Internal Server Error');
        } else{
            header('HTTP/1.1 201 Created');
            echo json_encode(array("success" => true));
        }
    } else {
        header('HTTP/1.1 409 Conflict');
        echo json_encode(array("success" => false, "error" => "user already registered."));
    }
} else{
    echo json_encode(array("success" => false));
    header('HTTP/1.1 400 Bad Request');
}