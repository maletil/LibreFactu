<?php
/**
 * Date: 10/08/18
 * Time: 22:52
 */

header('Content-Type: application/json');

if (isset($_GET["username"]) && isset($_GET["password"])) {
    $username = "root";
    $password = "";
    $host = "localhost";
    $dbname = "testing";

    $conn = new mysqli($host, $username, $password, $dbname);
    mysqli_set_charset($conn, "latin1_swedish_ci");

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } else {
        //echo "Connected.";
    }

    $sql = "SELECT * FROM users WHERE `username` = '" . $_GET["username"] . "'";
    if (!$result = mysqli_query($conn, $sql)) {
        echo(json_encode(array("success" => false)));
        header('HTTP/1.1 500');
        die();
    }
    $sqlArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $sqlArray[] = $row;
    }

    if (isset($_GET["type"])) {$type = $_GET["type"];} else{$type = 1;}

    if (password_verify($_GET["password"], $sqlArray[0]["password"])) {
        //echo json_encode(array("success" => true));
        if (isset($_GET["new_username"])) {$username = $_GET["new_username"];} else{$username = $_GET["username"];}
        if (isset($_GET["new_password"])) {$password = password_hash($_GET["new_password"], PASSWORD_DEFAULT);} else{$password = $sqlArray[0]["password"];}
        $sql = "UPDATE users SET `username`= '".$username."', `password`= '".$password."', `type` = '".$type."' WHERE `username` = '".$_GET["username"]."'";
        if (sizeOf($sqlArray) == 1){
            if (!$result = mysqli_query($conn, $sql)) {
                echo json_encode(array("success" => false));
                header('HTTP/1.1 500');
            } else{
                echo json_encode(array("success" => true));
            }
        } else {
            header('HTTP/1.1 404 Not Found');
            echo json_encode(array("success" => false, "error" => "user not registered."));
        }
    } else{
        header('HTTP/1.1 401 Unauthorized');
        echo json_encode(array("success" => false));
        die();
    }
} else{
    echo json_encode(array("success" => false));
    header('HTTP/1.1 400 Bad Request');
}