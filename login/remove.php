<?php
/**
 * Date: 10/08/18
 * Time: 22:33
 */

header('Content-Type: application/json');

if (isset($_GET["username"]) && isset($_GET["password"])) {
    $username = "root";
    $password = "";
    $host = "localhost";
    $dbname = "testing";

    $conn = new mysqli($host, $username, $password, $dbname);
    mysqli_set_charset($conn, "latin1_swedish_ci");

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } else {
        //echo "Connected.";
    }

    $sql = "SELECT * FROM users WHERE `username` = '" . $_GET["username"] . "'";
    if (!$result = mysqli_query($conn, $sql)) {
        echo(json_encode(array("success" => false)));
        header('HTTP/1.1 500 Internal Server Error');die();
    }
    $sqlArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $sqlArray[] = $row;
    }

    if (sizeof($sqlArray) == 1){
        if (password_verify($_GET["password"], $sqlArray[0]["password"])) {
            $sql = "DELETE FROM `users` WHERE `username` = '" . $_GET["username"] . "'";
            if (!$result = mysqli_query($conn, $sql)) {
                echo json_encode(array("success" => false));
                header('HTTP/1.1 500 Internal Server Error');
            } else {
                echo json_encode(array("success" => true));
            }
        } else{
            header('HTTP/1.1 401 Unauthorized');
            echo json_encode(array("success" => false));
        }
    } else{
        header('HTTP/1.1 404 Not Found');
        echo json_encode(array("success" => false));
    }
} else{
    echo json_encode(array("success" => false));
    header('HTTP/1.1 400 Bad Request');
}