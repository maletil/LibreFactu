<?php
/**
 * Date: 7/08/18
 * Time: 0:43
 */


header('Content-Type: application/json');

if($_SERVER['REQUEST_METHOD'] === 'DELETE'){
    if (isset($_GET["Codigo"]) && is_numeric($_GET["Codigo"])) {
        require "../../utils/db_query.php";

        $cCodigo = $_GET["Codigo"];
        $sql = "SELECT * FROM Clientes WHERE Codigo = $cCodigo";
        $result = json_decode(sqlRequest($sql), true);
        if ($result["success"] == true) {
            if ($result["entries"] != 0) {
                $sql = "DELETE FROM `Clientes` WHERE Codigo = " . $cCodigo;
                if (sqlRequest($sql, "delete")) {
                    echo json_encode(array("success" => true, "deleted" => $result["output"]));
                }
            } else {
                echo json_encode(array("success" => false, 'error' => "client doesn't exist"));
            }
        }
    } else{
        echo json_encode(array("success" => false,'error' => "not empty Codigo is required"));
    }
} else{
    echo json_encode(array("success" => false,'error' => "use DELETE method"));
}