<?php
/**
 * Date: 7/08/18
 * Time: 0:16
 */


header('Content-Type: application/json');

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_GET["Codigo"]) && is_numeric($_GET["Codigo"])) {
        require "../../utils/db_query.php";

        $cCodigo = $_GET["Codigo"];
        $sql = "SELECT * FROM Clientes WHERE Codigo = $cCodigo";
        $result = json_decode(sqlRequest($sql), true);
        if ($result["success"] == true) {
            if ($result["entries"] == 0) {
                $sql = "INSERT INTO `Clientes`(`Codigo`, `Nombre`, `Tipo`, `DNI`, `Codigo Postal`, `Poblacion`, `Domicilio`) VALUES (" . $cCodigo . ",\"Franciscano Gomez\",0,\"12354314F\",42231,\"Tudián\",\"C/ Sin Nombre\")";
                if (sqlRequest($sql, "post")) {
                    echo json_encode(array("success" => true));
                }
            } else {
                echo json_encode(array("success" => false, 'error' => "client already exists", "output" => $result["output"]));
            }
        }
    } else{
        echo json_encode(array("success" => false,'error' => "not empty Codigo is required"));
    }
} else{
    echo json_encode(array("success" => false,'error' => "use POST method"));
}