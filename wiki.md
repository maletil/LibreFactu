## LibreFactu
This repository includes the REST API and the web interface _(wip)_

Api structure:
```
├── api // API root directory
│   ├── delete //DELETE METHOD
│   │   └── clientes.php
│   ├── get //GET METHOD
│   │   └── clientes.php
│   └── post //POST METHOD
│       └── clientes.php
├── index.php //API Version and Info 
├── private //Configuration
│   └── config.php
├── README.md
├── utils //PHP functions
│   └── db_query.php

```
This wiki is _also_ written in spanish.

## API

### GET

| Path       | Descripción           |
| ------------- |:-------------:|
|  Clientes    | Listado de clientes o cliente específico |
| col 2 is      | centered      |
| zebra stripes | are neat      |

#### clientes.php

