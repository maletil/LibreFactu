<?php
/**
 * Date: 10/08/18
 * Time: 22:49
 */

header('Content-Type: application/json');

session_start();
if (isset($_SESSION["SESSIONID"]) && $_SESSION["username"]) {
    $sessionID = ($_SESSION["SESSIONID"]);
    $username = ($_SESSION["username"]);
    echo json_encode(array("success" => true, "output" => array("SESSIONID" => $sessionID, "username" => $username)));
} else{
    echo json_encode(array("success" => false));
    header('HTTP/1.1 401 Unauthorized');
}