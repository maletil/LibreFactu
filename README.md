## LibreFactu _(WIP)_
A free (as in freedom) billing and accounting api with web interface written in php.

This program isn't intended for professional use as I'm coding it as a hobby, use it under your own risk!

This project's main aims are simplicity and speed. Security isn't really implemented **yet**.

```
Technologies used in API:
  php, mysqli, apache, REST.
```

## Documentation

All code is commented on this repo wiki, as I can edit it separately and I find it's clearer this way.  

##### Note: This program database structure is written in spanish.

## Installation

Just clone this repository and add your database configuration under `private/config.php`

#### Features:

- Clients, articles and bills management **_WIP_**
- PDF invoice generator. **_Not even started_**

