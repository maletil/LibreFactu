<?php
/**
 * Date: 6/08/18
 * Time: 23:34
 */

$basedir = realpath(__DIR__);
$configs = require $basedir."/../private/config.php";

function mysqlDBConnect () {
    global $configs;
    if ($configs !== null) {
        $username = $configs['username'];
        $password = $configs['password'];
        $host = $configs['host'];
        $dbname = $configs['dbname'];

        $conn = new mysqli($host, $username, $password, $dbname);
        mysqli_set_charset($conn, "latin1_swedish_ci");

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    } else {
        echo json_encode(array("success" => false,'error' => 'no config file provided'));
        die();
    }
}

function mysqlDBDisconnect ($conn) {
    $close = mysqli_close($conn);
    if(!$close) {
        echo json_encode(array("success" => false,'error' => 'could not disconnect.'));
    }
}

function sqlRequest ($sql, $mode = null){ // Returns json
    $conn = mysqlDBConnect();
    if(!$result = mysqli_query($conn, $sql)) return(array("success" => false));

    if ($mode == null) {
        $sqlArray = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $sqlArray[] = $row;
        }

        $rows = sizeof($sqlArray);
        mysqlDBDisconnect($conn);
        if ($rows == 0) {
            $outputArray = array("success" => true, "entries" => 0);
        } else {
            $outputArray = array("success" => true, "entries" => $rows, "output" => $sqlArray);
        }
        return json_encode($outputArray, JSON_UNESCAPED_UNICODE);
    } else{
        return true;
    }
}
